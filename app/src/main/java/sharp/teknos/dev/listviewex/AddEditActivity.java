package sharp.teknos.dev.listviewex;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddEditActivity extends AppCompatActivity implements View.OnClickListener{


    private EditText firstNameEdit;
    private EditText lastNameEdit;
    private EditText subjectEdit;
    private EditText markEdit;
    private int position = Util.NEW_ITEM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        firstNameEdit = (EditText)findViewById(R.id.firstNameEdit);
        lastNameEdit = (EditText)findViewById(R.id.lastNameEdit);
        subjectEdit = (EditText)findViewById(R.id.subjectEdit);
        markEdit = (EditText)findViewById(R.id.markEdit);
        Button saveBtn = (Button)findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Add");
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (bundle != null){
            //Edit
            actionBar.setTitle("Edit");
            position = bundle.getInt(Util.INDEX);
            firstNameEdit.setText(bundle.getString(Util.FIRST_NAME));
            lastNameEdit.setText(bundle.getString(Util.LAST_NAME));
            subjectEdit.setText(bundle.getString(Util.SUBJECT));
            markEdit.setText(bundle.getString(Util.MARK));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent();
        intent.putExtra(Util.INDEX,position);
        intent.putExtra(Util.FIRST_NAME,firstNameEdit.getText().toString());
        intent.putExtra(Util.LAST_NAME,lastNameEdit.getText().toString());
        intent.putExtra(Util.SUBJECT,subjectEdit.getText().toString());
        intent.putExtra(Util.MARK,markEdit.getText().toString());
        setResult(Util.ADD_EDIT_RES_CODE,intent);

        finish();
    }
}
