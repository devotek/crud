package sharp.teknos.dev.listviewex;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,AdapterView.OnItemLongClickListener {

    //private String[] students = new String[]{"Ram","Rajesh","Shiva","Shilpa","Sanjay"};
    private ArrayList<HashMap<String,String>> students = new ArrayList<HashMap<String,String>>();
    private SimpleAdapter simpleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("CRUD");
        actionBar.setSubtitle("Add/Edit Records");

        ListView listView = (ListView)findViewById(R.id.listView);

        HashMap<String,String> student = new HashMap<>();
        student.put(Util.FIRST_NAME,"Vasu");
        student.put(Util.LAST_NAME,"Dev");
        student.put(Util.SUBJECT,"English");
        student.put(Util.MARK,"32");

        students.add(student);

        student = new HashMap<>();
        student.put(Util.FIRST_NAME,"Shiva");
        student.put(Util.LAST_NAME,"Kumar");
        student.put(Util.SUBJECT,"History");
        student.put(Util.MARK,"65");

        students.add(student);

        simpleAdapter = new SimpleAdapter(this,
                students,
                R.layout.list_item_map,
                new String[]{Util.FIRST_NAME,Util.SUBJECT},
                new int[]{R.id.firstNameView,R.id.subjectView});
        listView.setAdapter(simpleAdapter);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.list_item,students);
        //listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        */

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == R.id.action_add){
            Intent intent = new Intent(this,AddEditActivity.class);
            startActivityForResult(intent,Util.ADD_EDIT_REQ_CODE);
        }else if (menuId == R.id.action_about){

        }else if (menuId == R.id.action_contact){

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //
        //Log.w(Util.TAG,students[position]);
        //Util.displayToast(this,students[position]);
        String firstName = students.get(position).get(Util.FIRST_NAME);
        Util.displayToast(this,firstName);
        Intent intent = new Intent(this,AddEditActivity.class);
        intent.putExtra(Util.INDEX,position);
        intent.putExtra(Util.FIRST_NAME,students.get(position).get(Util.FIRST_NAME));
        intent.putExtra(Util.LAST_NAME,students.get(position).get(Util.LAST_NAME));
        intent.putExtra(Util.SUBJECT,students.get(position).get(Util.SUBJECT));
        intent.putExtra(Util.MARK,students.get(position).get(Util.MARK));
        startActivityForResult(intent,Util.ADD_EDIT_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Util.ADD_EDIT_REQ_CODE){
            //AddEditActivity Destroyed
            if (resultCode == Util.ADD_EDIT_RES_CODE){
                Bundle bundle = data.getExtras();
                HashMap<String,String> student = new HashMap<String, String>();
                student.put(Util.FIRST_NAME,bundle.getString(Util.FIRST_NAME));
                student.put(Util.LAST_NAME,bundle.getString(Util.LAST_NAME));
                student.put(Util.SUBJECT,bundle.getString(Util.SUBJECT));
                student.put(Util.MARK,bundle.getString(Util.MARK));

                if (bundle.getInt(Util.INDEX) == Util.NEW_ITEM){
                    students.add(student);
                }else{
                    students.set(bundle.getInt(Util.INDEX),student);
                }

                simpleAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete?");
        builder.setMessage("Do you really want to delete this entry?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                students.remove(position);
                simpleAdapter.notifyDataSetChanged();
            }
        });



        builder.setNegativeButton("Cancel",null);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();


        return true;
    }
}
