package sharp.teknos.dev.listviewex;

import android.content.Context;
import android.widget.Toast;

public class Util {
    public static final String TAG = "ListViewApp";
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String SUBJECT = "subject";
    public static final String MARK = "mark";
    public static final String INDEX = "index";
    public static final int NEW_ITEM = -1;

    public static int ADD_EDIT_REQ_CODE = 32;
    public static int ADD_EDIT_RES_CODE = 33;

    public static void displayToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
